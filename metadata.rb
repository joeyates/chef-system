name             "system"
maintainer       "Joe Yates"
maintainer_email "joe.g.yates@gmail.com"
license          "MIT"
description      "Installs/Configures a system-wide configuration for a user's compter"
long_description IO.read(File.expand_path("README.md", __dir__))
version          "0.1.0"

depends "java"
