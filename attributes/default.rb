default["java"]["jdk_version"] = "8"
default["system"]["elasticsearch"]["repository"]["url"] =
  "https://artifacts.elastic.co/packages/5.x/apt"
