resource_name :system_mysql

property :name, String, default: "mysql"

actions :install
default_action :install

action :install do
  package "libmysqlclient-dev"
end
