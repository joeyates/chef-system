resource_name :system_java

property :name, String, default: "java"

actions :install
default_action :install

action :install do
  include_recipe "java"
end
