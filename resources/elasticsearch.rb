resource_name :system_elasticsearch

property :name, String, default: "elasticsearch"

actions :install
default_action :install

action :install do
  apt_repository "elasticsearch" do
    uri node["system"]["elasticsearch"]["repository"]["url"]
    key "https://artifacts.elastic.co/GPG-KEY-elasticsearch"
    distribution "stable"
    components ["main"]
    not_if { node["platform"] == "mac_os_x" }
  end

  package "elasticsearch"
end
