resource_name :system_redis

property :name, String, default: "redis"

actions :install
default_action :install

action :install do
  package "redis" do
    only_if { node["platform"] == "mac_os_x" }
  end

  package "redis-server" do
    not_if { node["platform"] == "mac_os_x" }
  end
end
